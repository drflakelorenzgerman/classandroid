package ir.ali.classandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        Log.d("Here 2","onCreate")
    }

    override fun onStart() {
        super.onStart()
        Log.d("Here 2","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Here 2","onResume")
    }


    override fun onPause() {
        super.onPause()
        Log.d("Here 2","onPause")
    }


    override fun onStop() {
        super.onStop()
        Log.d("Here 2","onStop")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d("Here 2","onDestroy")
    }


    override fun onRestart() {
        super.onRestart()
        Log.d("Here 2","onRestart")
    }
}