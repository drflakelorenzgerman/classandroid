package ir.ali.classandroid

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("Here","onCreate")

        txt.text = "Hello"
        txt.setTextColor(Color.parseColor("#00FF00"))
        txt.setTextColor(resources.getColor(R.color.black))

        btn.setOnClickListener {

        }

        check.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                txt.text = "CHECKED"
            }else{
                txt.text = "UnChecked"
            }
        }

        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if(fromUser){
                    progressBar.progress = progress
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        radioGorup.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rd1->{
                    txt.text = "Radio 1"
                }
                R.id.rd2->{
                    txt.text = "Radio 2"
                }
            }
        }

        btn.setOnClickListener {"Hello"
            edit.text.toString()
            val intent = Intent(this,SecondActivity::class.java)
            startActivity(intent)
        }
//
//        findViewById<Button>(R.id.btn).text = "2"
//
//        findViewById<Button>(R.id.btn).setOnClickListener(object : View.OnClickListener{
//            override fun onClick(v: View?) {
//                // Do sth
//                val intent = Intent(this@MainActivity,SecondActivity::class.java)
//                startActivity(intent)
//            }
//        })

//        findViewById<Button>(R.id.btn).setOnClickListener {
//
//        }


    }

    override fun onStart() {
        super.onStart()
        Log.d("Here","onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Here","onResume")
    }


    override fun onPause() {
        super.onPause()
        Log.d("Here","onPause")
    }


    override fun onStop() {
        super.onStop()
        Log.d("Here","onStop")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d("Here","onDestroy")
    }


    override fun onRestart() {
        super.onRestart()
        Log.d("Here","onRestart")
    }
}